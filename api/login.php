<?php 
require_once('../config/koneksi.php');
include "response.php" ;
$response = new Response();

$email_login            = $_POST['email_login'];
$pass_login             = $_POST['pass_login'];

$cek_email = mysqli_query($conn, "SELECT * FROM user WHERE login_email = '$email_login' AND status_aktif  = 'Y'")->num_rows;

if ($cek_email == 0) {
    $response->code = 400;
    $response->message = 'This email is not registered, please register first.';
    $response->data = '';
    $response->json();
    die();
} else {
    $cek_password = mysqli_query($conn, "SELECT login_password FROM user WHERE login_email = '$email_login' AND status_aktif  = 'Y'")->fetch_assoc();
    if (password_verify($pass_login, $cek_password['login_password'])){
        $sql   = mysqli_query($conn, "SELECT * FROM user WHERE login_email = '$email_login' AND status_aktif  = 'Y'")->fetch_assoc();
        $result['iduser']          = $sql['iduser'];
        $result['nama']          = $sql['nama'];
        $result['telp']          = $sql['telp'];
        $result['login_email']          = $sql['login_email'];
        $result['url_image']          = $getimageuser . $sql['url_image'];
        $result['kode_referal']          = $sql['kode_referal'];
    } else {
        $response->code = 400;
        $response->message = 'Your password is wrong please try again.';
        $response->data = '';
        $response->json();
        die();
    }

    $response->code = 200;
    $response->message = 'Successfully Login.';
    $response->data = $result;
    $response->json();
    die(); 

}

mysqli_close($conn);
?>