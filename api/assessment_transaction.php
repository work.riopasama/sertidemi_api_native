<?php
require_once('../config/koneksi.php');
require 'PHPMailerold/PHPMailerAutoload.php';
include "response.php";
$response = new Response();

$iduser                     = $_POST['iduser'];
$status_payment             = $_POST['status_payment'];
$idassessment               = $_POST['idassessment'];
$total_harga_ticket_akhir   = $_POST['totalbayar'];
$idtransaksi_voucher        = $_POST['idvoucher'] ?? '';
$nama_sertifikat            = $_POST['nama_sertifikat'];
$idpayment = '';

if ($total_harga_ticket_akhir != 0) {
    $idpayment            = $_POST['idpayment'] ?? '';
}

$assessment  = mysqli_fetch_object($conn->query("SELECT * FROM assessment where idassessment =  '$idassessment'"));
$transaction = mysqli_fetch_object($conn->query("SELECT UUID_SHORT() as id"));

// $invoice = id_ke_struk(createID('invoice', 'transaksi', 'TR'));
$invoice = createInvoice('invoice', 'transaksi', 'TR');
if (!empty($idvoucher)) {
    if ($total_harga_ticket_akhir != 0) {
        $data = $conn->query("INSERT into transaksi SET idtransaksi = '$transaction->id',
        invoice = '$invoice',
        jenis_transaksi = 'assessment',
        iduser = '$iduser',
        status_transaksi = '1',    
        status_payment = '$status_payment',
        batas_pembayaran = DATE_ADD(NOW(), INTERVAL + 72 DAY),
        total_pembayaran = '$total_harga_ticket_akhir',
        idtransaksi_voucher = '$idtransaksi_voucher',
        payment_type = '$idpayment',
        nama_sertifikat = '$nama_sertifikat',
        jumlah_point = '$assessment->point_assessment',
        nama_point = '$assessment->point_assessment_nama'
        ");
    } else {
        $data = $conn->query("INSERT into transaksi SET idtransaksi = '$transaction->id',
        invoice = '$invoice',
        jenis_transaksi = 'assessment',
        iduser = '$iduser',
        status_transaksi = '7',    
        status_payment = '$status_payment',
        batas_pembayaran = DATE_ADD(NOW(), INTERVAL + 72 DAY),
        total_pembayaran = '$total_harga_ticket_akhir',
        idtransaksi_voucher = '$idtransaksi_voucher',
        nama_sertifikat = '$nama_sertifikat',
        jumlah_point = '$assessment->point_assessment',
        nama_point = '$assessment->point_assessment_nama'
        ");
    }

    if ($data) {
        $conn->begin_transaction();

        $data2[] = $conn->query("INSERT into transaksi_detail SET idtransaksi_detail = UUID_SHORT(),
        idtransaksi = '$transaction->id',
        idassessment = '$idassessment',
        harga_normal = '$assessment->harga_assessment',
        status_diskon = '$assessment->status_diskon',
        diskon = '$assessment->diskon_assessment'");

        $filesertifikat = $getsertifikat . "?idtransaksi=" . $transaction->id;

        $data2[] = $conn->query("INSERT into assessment_sertifikat SET idassessment_sertifikat = UUID_SHORT(),
        idassessment = '$idassessment',
        idtransaksi = '$transaction->id',
        status_sertifikat = 'N',
        link_sertifikat = '$filesertifikat'");

        $data2[] = $conn->query("INSERT into notifikasi SET idnotifikasi = UUID_SHORT(),
        idtransaksi = '$transaction->id',
        iduser = '$iduser',
        status_baca = 'N'
        ");

        if (in_array(false, $data2)) {
            $response->code = 400;
            $response->message = mysqli_error($conn);
            $response->data = '';
            $response->json();
            die();
        } else {
            $result['id_transaksi'] = $transaction->id;
            $result['no_invoice'] = $invoice;
            $result['total'] = $total_harga_ticket_akhir;
            $result['metode_pembayaran'] = $idpayment;
            $result['nama_assessment'] = $assessment->nama_assessment;
            $result['created_at'] = mysqli_fetch_object($conn->query("SELECT tanggal_input FROM transaksi WHERE idtransaksi = '$transaction->id'"))->tanggal_input;
            $conn->commit();
            $response->code = 200;
            $response->message = 'success';
            $response->data = $result;
            $response->json();
            die();
        }
    } else {
        $response->code = 400;
        $response->message = mysqli_error($conn);
        $response->data = [];
        $response->json();
        die();
    }
} else {
    if ($total_harga_ticket_akhir != 0) {
        $data = $conn->query("INSERT into transaksi SET idtransaksi = '$transaction->id',
        invoice = '$invoice',
        jenis_transaksi = 'assessment',
        iduser = '$iduser',
        status_transaksi = '1',    
        status_payment = '$status_payment',
        batas_pembayaran = DATE_ADD(NOW(), INTERVAL + 72 DAY),
        total_pembayaran = '$total_harga_ticket_akhir',
        payment_type = '$idpayment',
        nama_sertifikat = '$nama_sertifikat',
        jumlah_point = '$assessment->point_assessment',
        nama_point = '$assessment->point_assessment_nama'
        ");
    } else {
        $data = $conn->query("INSERT into transaksi SET idtransaksi = '$transaction->id',
        invoice = '$invoice',
        jenis_transaksi = 'assessment',
        iduser = '$iduser',
        status_transaksi = '7',    
        status_payment = '$status_payment',
        batas_pembayaran = DATE_ADD(NOW(), INTERVAL + 72 DAY),
        total_pembayaran = '$total_harga_ticket_akhir',
        nama_sertifikat = '$nama_sertifikat',
        jumlah_point = '$assessment->point_assessment',
        nama_point = '$assessment->point_assessment_nama'
        ");
    }

    if ($data) {
        $conn->begin_transaction();

        $data2[] = $conn->query("INSERT into transaksi_detail SET idtransaksi_detail = UUID_SHORT(),
        idtransaksi = '$transaction->id',
        idassessment = '$idassessment',
        harga_normal = '$assessment->harga_assessment',
        status_diskon = '$assessment->status_diskon',
        diskon = '$assessment->diskon_assessment'
        ");

        $filesertifikat = $getsertifikat . "?idtransaksi=" . $transaction->id;

        $data2[] = $conn->query("INSERT into assessment_sertifikat SET idassessment_sertifikat = UUID_SHORT(),
        idassessment = '$idassessment',
        idtransaksi = '$transaction->id',
        status_sertifikat = 'N',
        link_sertifikat = '$filesertifikat'");

        $data2[] = $conn->query("INSERT into notifikasi SET idnotifikasi = UUID_SHORT(),
        idtransaksi = '$transaction->id',
        iduser = '$iduser',
        status_baca = 'N'
        ");



        // $data2 = $conn->query("INSERT into transaksi_detail SET idtransaksi_detail = UUID_SHORT(),
        //      idtransaksi = '$transaction->id',
        //      idassessment = '$idassessment',
        //      harga_normal = '$assessment->harga_assessment',
        //      status_diskon = '$assessment->status_diskon',
        //      diskon = '$assessment->diskon_assessment'");

        if (in_array(false, $data2)) {
            $response->code = 400;
            $response->message = mysqli_error($conn);
            $response->data = '';
            $response->json();
            die();
        } else {
            $result['id_transaksi'] = $transaction->id;
            $result['no_invoice'] = $invoice;
            $result['total'] = $total_harga_ticket_akhir;
            $result['metode_pembayaran'] = $idpayment;
            $result['nama_assessment'] = $assessment->nama_assessment;
            $result['created_at'] = mysqli_fetch_object($conn->query("SELECT tanggal_input FROM transaksi WHERE idtransaksi = '$transaction->id'"))->tanggal_input;
            $conn->commit();
            $response->code = 200;
            $response->message = 'success';
            $response->data = $result;
            $response->json();
            die();
        }
    } else {
        $response->code = 400;
        $response->message = mysqli_error($conn);
        $response->data = '';
        $response->json();
        die();
    }
}


mysqli_close($conn);

// $conn->begin_transaction();
// $invoice = id_ke_struk(createID('invoice', 'transaksi', 'TR'));
// if (!empty($idvoucher)) {
//     $data[] = $conn->query("INSERT into transaksi SET idtransaksi = '$transaction->id',
//         invoice = '$invoice',
//         jenis_transaksi = 'assessment',
//         iduser = '$iduser',
//         status_transaksi = '1',
//         status_payment = '$status_payment',
//         batas_pembayaran = DATE_ADD(NOW(), INTERVAL + 72 DAY),
//         total_pembayaran = '$total_harga_ticket_akhir',
//         idtransaksi_voucher = '$idtransaksi_voucher',
//         nama_sertifikat = '$nama_sertifikat',
//     ");

//     $data[] = $conn->query("INSERT into transaksi_detail SET idtransaksi_detail = UUID_SHORT(),
//         idtransaksi = '$transaction->id',
//         idassessment = '$idassessment',
//         harga_normal = '$assessment->harga_assessment',
//         status_diskon = '$assessment->status_diskon',
//         diskon = '$assessment->diskon_assessment'
//     ");
// } else {
//     $data[] = $conn->query("INSERT into transaksi SET idtransaksi = '$transaction->id',
//     invoice = '$invoice',
//     jenis_transaksi = 'assessment',
//     iduser = '$iduser',
//     status_transaksi = '1',
//     status_payment = '$status_payment',
//     batas_pembayaran = DATE_ADD(NOW(), INTERVAL + 72 DAY),
//     total_pembayaran = '$total_harga_ticket_akhir',
//     nama_sertifikat = '$nama_sertifikat'
// ");
//     $data[] = $conn->query("INSERT into transaksi_detail SET idtransaksi_detail = UUID_SHORT(),
//     idtransaksi = '$transaction->id',
//     idassessment = '$idassessment',
//     harga_normal = '$assessment->harga_assessment',
//     status_diskon = '$assessment->status_diskon',
//     diskon = '$assessment->diskon_assessment'
// ");
// }

// if (in_array(false, $data)) {
//     $response->code = 400;
//     $response->message = mysqli_error($conn);
//     $response->data = '';
//     $response->json();
//     die();
// } else {
//     $result['id_transaksi'] = $transaction->id;
//     $result['no_invoice'] = $invoice;
//     $result['total'] = $total_harga_ticket_akhir;

//     $conn->commit();
//     $response->code = 200;
//     $response->message = 'success';
//     $response->data = $result;
//     $response->json();
//     die();
// }
