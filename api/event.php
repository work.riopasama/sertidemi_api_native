<?php
require_once "../config/koneksi.php";
include "response.php";

$response = new Response();

$search = $_GET['q'] ?? '';

if (isset($_GET['id_event'])) {
    $eventID = $_GET['id_event'] ?? sendError();

    $data = mysqli_fetch_object($conn->query("SELECT * FROM event e 
    JOIN user_bo bo ON e.iduser_bo = bo.iduser_bo
    WHERE idevent = '$eventID'"));

    // $sponsores = $conn->query("SELECT * FROM sponsor WHERE idevent = '$data->idevent' ORDER BY urutan_banner ASC;");
    // $sponsores = $conn->query("SELECT * FROM sponsor WHERE idevent = '$data->idevent' ORDER BY urutan_banner ASC;");
    $banner = $conn->query("SELECT * FROM event_banners WHERE idevent = '$data->idevent'");

    $banners = array();
    // $sponsoress = array();

    while ($key = mysqli_fetch_object($banner)) {
        array_push($banners, array(
            'ideventbanner' => $key->ideventbanner,
            'link_banner' => $getimagebanner . $key->link_banner,
            'status_banner' => $key->status_banner,
            'idevent' => $key->idevent,
        ));
    }

    $listsponsor = array();
    $sponsores = $conn->query("SELECT * FROM sponsor WHERE idevent = '$eventID'");
    foreach ($sponsores as $key => $value) {
        array_push($listsponsor, array(
            'idsponsor' => $value['idsponsor'],
            'idevent' => $value['idevent'],
            'idassessment' => $value['idassessment'],
            'nama_sponsor' => $value['nama_sponsor'],
            'url_logo_sponsor' => $getimagesponsor . $value['icon_sponsor'],
            'urutan_sponsor' => $value['urutan_sponsor'],
        ));
    }


    if (($data->diskon) == 0) {
        $harga_diskon = 0;
    } else {
        if ($data->status_diskon == '1') {
            $harga_diskon = $data->harga - ($data->harga * ($data->diskon / 100));
        } else if ($data->status_diskon == '2') {
            $harga_diskon = $data->harga - $data->diskon;
        } else {
            $harga_diskon = 0;
        }
    }

    $result['idevent']          = $data->idevent;
    $result['banners']          = $banners;
    $result['nama_event']       = $data->nama_event;
    $result['idevent_kategori'] = $data->idevent_kategori;
    $result['deskripsi_event']  = $data->deskripsi_event;
    $result['link_status']      = $data->link_status;
    $result['link_meeting']     = $data->link_meeting;
    $result['event_mulai']      = $data->event_mulai;
    $result['event_selesai']    = $data->event_selesai;
    $result['tanggal_input']    = $data->tanggal_input;
    $result['url_image_panjang'] = $getimageevent . $data->url_image_panjang;
    $result['url_image_kotak']  = $getimageevent . $data->url_image_kotak;
    $result['harga']            = $data->harga;
    $result['diskon']           = $data->diskon;
    $result['harga_diskon']     = (string)$harga_diskon;
    $result['status_diskon']    = $data->status_diskon;
    $result['penulis']          = $data->nama;
    $result['sponsor']          = $listsponsor;
    $result['sponsor_utama']    = $getimageevent . $data->url_logo_sponsor;
    $result['point_event']      = $data->point_event . " " . $data->point_event_nama;

    if ($data) {
        $response->code = 200;
        $response->message = 'found';
        $response->data = $result;
        $response->json();
        die();
    } else {
        $response->code = 400;
        $response->message = mysqli_error($conn);
        $response->data = '';
        $response->json();
        die();
    }
} else {
    $categoriID = $_GET['id_kategori'] ?? sendError();
    $offset = $_GET['offset'] ?? sendError();

    $datalist = array();
    $data = $conn->query("SELECT * FROM event WHERE idevent_kategori = '$categoriID' and nama_event LIKE '%$search%' and CURRENT_TIMESTAMP <= event_selesai LIMIT 10 OFFSET $offset;");
    foreach ($data as $key => $value) {
        if ($value['diskon'] == 0) {
            $harga_diskon = 0;
        } else {
            if ($value['status_diskon'] == '1') {
                $harga_diskon = $value['harga'] - $value['harga'] * ($value['diskon'] / 100);
            } else if ($value['status_diskon'] == '2') {
                $harga_diskon = $value['harga'] - $value['diskon'];
            } else {
                $harga_diskon = 0;
            }
        }
        array_push($datalist, array(
            'id' => $value['idevent'],
            'nama' => $value['nama_event'],
            'mulai' => $value['event_mulai'],
            'selesai' => $value['event_selesai'],
            'url_image_panjang' => !empty($value['url_image_panjang']) ? $getimageevent . $value['url_image_panjang'] : '',
            'url_image_kotak' => !empty($value['url_image_kotak']) ? $getimageevent . $value['url_image_panjang'] : '',
            'harga' => $value['harga'],
            'diskon' => $value['diskon'],
            'status_diskon'  => $value['status_diskon'],
            'harga_diskon' => (string)$harga_diskon,
            'status' => 'event',
            'sponsor_utama' => $getimageevent . $value['url_logo_sponsor'],
        ));
    }

    if ($data) {
        $response->code = 200;
        $response->message = 'found';
        $response->data = $datalist;
        $response->json();
        die();
    } else {
        $response->code = 400;
        $response->message = mysqli_error($conn);
        $response->data = '';
        $response->json();
        die();
    }
}


function sendError()
{
    $response = new Response();
    $response->code = 400;
    $response->message = 'bad request';
    $response->data = '';
    $response->json();
    die();
}
