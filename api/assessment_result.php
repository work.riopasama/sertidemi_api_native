<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$idassessment = $_GET['idassessment'] ?? SendError();
$iduser     = $_GET['iduser'] ?? sendError();
$idtransaksi = $_GET['idtransaksi'] ?? '';

$pilgan = mysqli_fetch_object($conn->query("SELECT * FROM assessment_jawaban_pilgan ajp
JOIN assessment ass ON ajp.idassessment = ass.idassessment 
LEFT JOIN transaksi tr ON ajp.idtransaksi = tr.idtransaksi
WHERE ass.idassessment = '$idassessment' AND ajp.iduser = '$iduser' AND tr.idtransaksi = '$idtransaksi'"));
$kriteria = new Kriteria();
$kriteria->nilai = $pilgan->nilai;
$kriteria->obj = $conn->query("SELECT * FROM kriteria_nilai ORDER BY batas_ambang DESC;")->fetch_all(MYSQLI_ASSOC);

$resp['idassessment'] = $pilgan->idassessment;
$resp['nama_assessment'] = $pilgan->nama_assessment;
$resp['nilai'] = $pilgan->nilai;
$resp['comment'] = $kriteria->getDeskripsi();
$resp['hasil'] = $kriteria->getHasil();
$resp['duration'] = $pilgan->lama_dikerjakan;
$resp['nama_sertifikat'] = $pilgan->nama_sertifikat;
$resp['nama_user'] = $pilgan->nama;
$resp['submit_at'] = $pilgan->tgl_input_time;



$response->code = 200;
$response->message = 'found';
$response->data = $resp;
$response->json();
die();


function sendError()
{
    $response = new Response();
    $response->code = 400;
    $response->message = 'bad request';
    $response->data = '';
    $response->json();
    die();
}

class Kriteria
{
    public $nilai;
    public $obj;
    private $hasil, $deskripsi;

    function getHasil()
    {
        foreach ($this->obj as $key => $value) {
            if ($this->nilai <= $value['batas_ambang']) {
                $this->hasil = $value['keterangan'];
                $this->deskripsi = $value['deskripsi'];
            }
        }
        return $this->hasil;
    }

    function getDeskripsi()
    {
        foreach ($this->obj as $key => $value) {
            if ($this->nilai <= $value['batas_ambang']) {
                $this->hasil = $value['keterangan'];
                $this->deskripsi = $value['deskripsi'];
            }
        }
        return $this->deskripsi;
    }
}
