<?php
require_once('../config/koneksi.php');
include "response.php";

$response = new Response();

$tag   = $_GET['tag'] ?? '';

switch ($tag) {
    case "event":
        $dataevent = $conn->query("SELECT * FROM event where NOW() <= event_selesai ORDER BY event.tanggal_input DESC");
        $dataeventlist = array();
        foreach ($dataevent as $key => $value) {
            if ($value['status_diskon'] == '1') {
                $harga_diskon = $value['harga'] - ($value['harga'] * ($value['diskon'] / 100));
            } else if ($value['status_diskon'] == '2') {
                $harga_diskon = $value['harga'] - $value['diskon'];
            } else {
                $harga_diskon = 0;
            }
            array_push($dataeventlist, array(
                'id' => $value['idevent'],
                'nama' => $value['nama_event'],
                'mulai' => $value['event_mulai'],
                'selesai' => $value['event_selesai'],
                'harga' => $value['harga'],
                'status_diskon'     => $value['status_diskon'],
                'diskon' => $value['diskon'],
                'harga_diskon' => $harga_diskon,
                'url_image_panjang' => $getimageevent . $value['url_image_panjang'],
                'url_image_kotak' => $getimageevent . $value['url_image_kotak'],
                'status' => 'event',
                'sponsor_utama' => $getimageevent . $value['url_logo_sponsor'],
            ));
        }

        if (isset($dataeventlist[0])) {
            $response->code = 200;
            $response->message = 'result';
            $response->data = $dataeventlist;
            $response->json();
            die();
        } else {
            $response->code = 200;
            $response->message = 'Tidak ada data ditampilkan.';
            $response->data = [];
            $response->json();
            die();
        }
        break;
    case "assessment":
        $dataassessment = $conn->query("SELECT * FROM assessment where NOW() <= tanggal_batas_assessment ORDER BY tanggal_input DESC");
        $dataassessmentlist = array();
        foreach ($dataassessment as $key => $value) {
            if ($value['status_diskon'] == '1') {
                $harga_diskon = $value['harga_assessment'] - ($value['harga_assessment'] * ($value['diskon_assessment'] / 100));
            } else if ($value['status_diskon'] == '2') {
                $harga_diskon = $value['harga_assessment'] - $value['diskon_assessment'];
            } else {
                $harga_diskon = 0;
            }
            array_push($dataassessmentlist, array(
                'id' => $value['idassessment'],
                'nama' => $value['nama_assessment'],
                'mulai' => $value['tanggal_mulai_assessment'],
                'selesai' => $value['tanggal_batas_assessment'],
                'harga' => $value['harga_assessment'],
                'status_diskon'     => $value['status_diskon'],
                'diskon' => $value['diskon_assessment'],
                'harga_diskon' => $harga_diskon,
                'url_image_panjang' => $getimageassessment . $value['url_image_panjang'],
                'url_image_kotak' => $getimageassessment . $value['url_image_kotak'],
                'status' => 'assessment',
                'sponsor_utama' => $getimageassessment . $value['url_logo_sponsor'],
            ));
        }

        if (isset($dataassessmentlist[0])) {
            $response->code = 200;
            $response->message = 'result';
            $response->data = $dataassessmentlist;
            $response->json();
            die();
        } else {
            $response->code = 200;
            $response->message = 'Tidak ada data ditampilkan.';
            $response->data = [];
            $response->json();
            die();
        }
        break;
    case "eventterlaris":
        $dataeventterlaris = $conn->query("SELECT *, (SELECT COUNT(td.idtransaksi) FROM transaksi_detail td WHERE td.idevent = e.idevent) as jml_terlaris FROM event e
        WHERE NOW() <= e.event_selesai ORDER BY jml_terlaris DESC");
        $dataeventterlarislist = array();
        foreach ($dataeventterlaris as $key => $value) {
            if ($value['status_diskon'] == '1') {
                $harga_diskon = $value['harga'] - ($value['harga'] * ($value['diskon'] / 100));
            } else if ($value['status_diskon'] == '2') {
                $harga_diskon = $value['harga'] - $value['diskon'];
            } else {
                $harga_diskon = 0;
            }
            array_push($dataeventterlarislist, array(
                'id' => $value['idevent'],
                'nama' => $value['nama_event'],
                'mulai' => $value['event_mulai'],
                'selesai' => $value['event_selesai'],
                'harga' => $value['harga'],
                'status_diskon'     => $value['status_diskon'],
                'diskon' => $value['diskon'],
                'harga_diskon' => $harga_diskon,
                'url_image_panjang' => $getimageevent . $value['url_image_panjang'],
                'url_image_kotak' => $getimageevent . $value['url_image_kotak'],
                'terjual' => $value['jml_terlaris'],
                'status' => 'event',
                'sponsor_utama' => $getimageevent . $value['url_logo_sponsor'],
            ));
        }

        if (isset($dataeventterlarislist[0])) {
            $response->code = 200;
            $response->message = 'result';
            $response->data = $dataeventterlarislist;
            $response->json();
            die();
        } else {
            $response->code = 200;
            $response->message = 'Tidak ada data ditampilkan.';
            $response->data = [];
            $response->json();
            die();
        }
        break;
    case "assessmentterlaris":
        $dataassessmentterlaris = $conn->query("SELECT *, (SELECT COUNT(td.idtransaksi) FROM transaksi_detail td WHERE td.idassessment = a.idassessment) as jml_terlaris FROM assessment a
        WHERE NOW() <= a.tanggal_batas_assessment ORDER BY jml_terlaris DESC");
        $dataassessmentterlarislist = array();
        foreach ($dataassessmentterlaris as $key => $value) {
            if ($value['status_diskon'] == '1') {
                $harga_diskon = $value['harga_assessment'] - ($value['harga_assessment'] * ($value['diskon_assessment'] / 100));
            } else if ($value['status_diskon'] == '2') {
                $harga_diskon = $value['harga_assessment'] - $value['diskon_assessment'];
            } else {
                $harga_diskon = 0;
            }
            array_push($dataassessmentterlarislist, array(
                'id' => $value['idassessment'],
                'nama' => $value['nama_assessment'],
                'mulai' => $value['tanggal_mulai_assessment'],
                'selesai' => $value['tanggal_mulai_assessment'],
                'harga' => $value['harga_assessment'],
                'status_diskon'     => $value['status_diskon'],
                'diskon' => $value['diskon_assessment'],
                'harga_diskon' => $harga_diskon,
                'url_image_panjang' => $getimageassessment . $value['url_image_panjang'],
                'url_image_kotak' => $getimageassessment . $value['url_image_kotak'],
                'status' => 'assessment',
                'terjual' => $value['jml_terlaris'],
                'sponsor_utama' => $getimageassessment . $value['url_logo_sponsor'],
            ));
        }

        if (isset($dataassessmentterlarislist[0])) {
            $response->code = 200;
            $response->message = 'result';
            $response->data = $dataassessmentterlarislist;
            $response->json();
            die();
        } else {
            $response->code = 200;
            $response->message = 'Tidak ada data ditampilkan.';
            $response->data = [];
            $response->json();
            die();
        }
        break;
}
mysqli_close($conn);
