<?php
require_once "../config/koneksi.php";
include "response.php";

$idevent = $_POST['idevent'] ?? '';
$idassessment = $_POST['idassessment'] ?? '';
$idvoucher = $_POST['idvoucher'] ?? '';

$response = new Response();
if (!empty($idevent) && !empty($idvoucher)) {
    $data = $conn->query("SELECT * FROM event e 
    JOIN event_kategori ek ON e.idevent_kategori = ek.dievent_kategori 
    JOIN transaksi_voucher tv ON tv.idevent = e.idevent 
    WHERE tv.idtransaksi_voucher ='$idvoucher' AND e.idevent = '$idevent'");
    $data = mysqli_fetch_object($data);

    if (is_null($data)) {
        $response->code = 400;
        $response->message = 'Voucher tidak bisa dipakai!';
        $response->data = '';
        $response->json();
        die();
    }

    switch ($data->status_diskon) {
        case '1':
            $harga_akhir = $data->harga - (($data->diskon * $data->harga) / 100);
            break;

        case '2':
            $harga_akhir = ($data->harga - $data->diskon);
            break;
        default:
            $harga_akhir = $data->harga;
            break;
    }

    switch (strtolower($data->jenis_potongan)) {
        case 'persen':
            $potongan_voucher = ($data->nilai_potongan * $harga_akhir) / 100;
            $harga_akhir = $harga_akhir - $potongan_voucher;
            break;
        case 'rupiah':
            $potongan_voucher = $data->nilai_potongan;
            $harga_akhir = $harga_akhir - $potongan_voucher;
            break;
        default:
            $potongan_voucher = '';
            break;
    }

    $result['idevent']          = $data->idevent;
    $result['nama_event']       = $data->nama_event;
    $result['deskripsi_event']  = $data->deskripsi_event;
    $result['url_image_panjang'] = $data->url_image_panjang;
    $result['url_image_kotak']  = $data->url_image_kotak;
    $result['event_mulai']      = $data->event_mulai;
    $result['event_selesai']    = $data->event_selesai;
    $result['idevent_kategori'] = $data->idevent_kategori;
    $result['nama_kategori']    = $data->nama_kategori;
    $result['harga']            = $data->harga;
    $result['diskon_status']    = $data->status_diskon;
    $result['diskon']           = $data->diskon;
    $result['nama_voucher']     = $data->nama_voucher;
    $result['potongan_voucher'] = strval($potongan_voucher);
    $result['harga_akhir']      = $harga_akhir;

    $response->code = 200;
    $response->message = 'Voucher berhasil dipakai!';
    $response->data = $result;
} elseif (!empty($idassessment) && !empty($idvoucher)) {
    $data = $conn->query("SELECT * FROM assessment a 
    JOIN assessment_kategori ak ON a.idassessment_kategori = ak.idassessment_kategori
    JOIN transaksi_voucher tv ON tv.idassessment = a.idassessment 
    WHERE tv.idtransaksi_voucher = '$idvoucher' AND a.idassessment = '$idassessment';");
    $data = mysqli_fetch_object($data);

    if (is_null($data)) {
        $response->code = 400;
        $response->message = 'Voucher tidak bisa dipakai!';
        $response->data = '';
        $response->json();
        die();
    }

    switch ($data->status_diskon) {
        case '1':
            $harga_akhir = round($data->harga_assessment - (($data->diskon_assessment * $data->harga_assessment) / 100));
            break;

        case '2':
            $harga_akhir = ($data->harga_assessment - $data->diskon_assessment);
            break;
        default:
            $harga_akhir = $data->harga_assessment;
            break;
    }

    switch (strtolower($data->jenis_potongan)) {
        case 'persen':
            $potongan_voucher = round(($data->nilai_potongan * $harga_akhir) / 100);
            $harga_akhir = $harga_akhir - $potongan_voucher;
            break;
        case 'rupiah':
            $potongan_voucher = $data->nilai_potongan;
            $harga_akhir = $harga_akhir - $potongan_voucher;
            break;
        default:
            $potongan_voucher = '';
            break;
    }

    $result['idassessment']          = $data->idassessment;
    $result['nama_assessment']       = $data->nama_assessment;
    $result['deskripsi_assessment']  = $data->deskripsi_assessment;
    $result['url_image_panjang'] = $data->url_image_panjang;
    $result['url_image_kotak']  = $data->url_image_kotak;
    $result['assessment_mulai']      = $data->tanggal_mulai_assessment;
    $result['assessment_selesai']    = $data->tanggal_batas_assessment;
    $result['idassessment_kategori'] = $data->idassessment_kategori;
    $result['nama_kategori']    = $data->nama_kategori;
    $result['harga']            = $data->harga_assessment;
    $result['diskon_status']    = $data->status_diskon;
    $result['diskon']           = $data->diskon_assessment;
    $result['nama_voucher']     = $data->nama_voucher;
    $result['potongan_voucher'] = strval($potongan_voucher);
    $result['harga_akhir']      = $harga_akhir;

    $response->code = 200;
    $response->message = 'Voucher berhasil dipakai!';
    $response->data = $result;
} elseif (!empty($idevent)) {
    $data = mysqli_fetch_object($conn->query("SELECT * FROM event e
    LEFT JOIN event_kategori ek ON e.idevent_kategori = ek.dievent_kategori
    WHERE e.idevent ='$idevent'"));

    switch ($data->status_diskon) {
        case '1':
            $harga_akhir = $data->harga - (($data->diskon * $data->harga) / 100);
            break;

        case '2':
            $harga_akhir = ($data->harga - $data->diskon);
            break;
        default:
            $harga_akhir = $data->harga;
            break;
    }

    $result['idevent']          = $data->idevent;
    $result['nama_event']       = $data->nama_event;
    $result['deskripsi_event']  = $data->deskripsi_event;
    $result['url_image_panjang'] = $data->url_image_panjang;
    $result['url_image_kotak']  = $data->url_image_kotak;
    $result['event_mulai']      = $data->event_mulai;
    $result['event_selesai']    = $data->event_selesai;
    $result['idevent_kategori'] = $data->idevent_kategori;
    $result['nama_kategori']    = $data->nama_kategori;
    $result['harga']            = $data->harga;
    $result['diskon_status']    = $data->status_diskon;
    $result['diskon']           = $data->diskon;
    $result['nama_voucher']     = '';
    $result['potongan_voucher'] = '';
    $result['harga_akhir']      = $harga_akhir;

    $response->code = 200;
    $response->message = 'Ok';
    $response->data = $result;
} elseif (!empty($idassessment)) {
    $data = mysqli_fetch_object($conn->query("SELECT * FROM assessment a
        LEFT JOIN assessment_kategori ak ON a.idassessment_kategori = ak.idassessment_kategori
        WHERE a.idassessment  ='$idassessment'"));

    switch ($data->status_diskon) {
        case '1':
            $harga_akhir = $data->harga_assessment - (($data->diskon_assessment * $data->harga_assessment) / 100);
            break;

        case '2':
            $harga_akhir = ($data->harga_assessment - $data->diskon_assessment);
            break;
        default:
            $harga_akhir = $data->harga_assessment;
            break;
    }

    $result['idassessment']          = $data->idassessment;
    $result['nama_assessment']       = $data->nama_assessment;
    $result['deskripsi_assessment']  = $data->deskripsi_assessment;
    $result['url_image_panjang'] = $data->url_image_panjang;
    $result['url_image_kotak']  = $data->url_image_kotak;
    $result['assessment_mulai']      = $data->tanggal_mulai_assessment;
    $result['assessment_selesai']    = $data->tanggal_batas_assessment;
    $result['idassessment_kategori'] = $data->idassessment_kategori;
    $result['nama_kategori']    = $data->nama_kategori;
    $result['harga']            = $data->harga_assessment;
    $result['diskon_status']    = $data->status_diskon;
    $result['diskon']           = $data->diskon_assessment;
    $result['nama_voucher']     = '';
    $result['potongan_voucher'] = '';
    $result['harga_akhir']      = $harga_akhir;

    $response->code = 200;
    $response->message = 'Ok';
    $response->data = $result;
} else {
    $response->code = 400;
    $response->message = 'bad request';
    $response->data = '';
}
$response->json();
die();
