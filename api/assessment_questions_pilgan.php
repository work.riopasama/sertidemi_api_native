<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$idassessment = $_GET['idassessment'] ?? '';

$cek_assessment_pilgan = $conn->query("SELECT * FROM assessment WHERE idassessment = '$idassessment'")->fetch_assoc();
$tampil_assessment = $cek_assessment_pilgan['tampil_assessment_pilgan'];

$data = $conn->query("SELECT * FROM assessment_soal_pilgan WHERE idassessment = '$idassessment'");
$datalist = array();
while ($row = mysqli_fetch_array($data)) {
    array_push($datalist, array(
        'idassessment_soal_pilgan' => $row['idassessment_soal_pilgan'],
        'soal_text' => htmlspecialchars_decode($row['soal_text']),
        'soal_gambar' => htmlspecialchars_decode($row['soal_gambar']),
        'soal_gambar' => htmlspecialchars_decode($row['soal_gambar']),
        'pilihan_a_text' => htmlspecialchars_decode($row['pilihan_a_text']),
        'pilihan_a_gambar' => htmlspecialchars_decode($row['pilihan_a_gambar']),
        'pilihan_b_text' => htmlspecialchars_decode($row['pilihan_b_text']),
        'pilihan_b_gambar' => htmlspecialchars_decode($row['pilihan_b_gambar']),
        'pilihan_c_text' => htmlspecialchars_decode($row['pilihan_c_text']),
        'pilihan_c_gambar' => htmlspecialchars_decode($row['pilihan_c_gambar']),
        'pilihan_c_gambar' => htmlspecialchars_decode($row['pilihan_c_gambar']),
        'pilihan_d_text' => htmlspecialchars_decode($row['pilihan_d_text']),
        'pilihan_d_gambar' => htmlspecialchars_decode($row['pilihan_d_gambar']),
        'jawaban' => htmlspecialchars_decode($row['jawaban']),
        'penjelasan_jawaban' => htmlspecialchars_decode($row['pilihan_d_gambar']),
    ));
}  
shuffle($datalist);
$datalist2 = array();

for ($i = 0; $i < $tampil_assessment; $i++) {
    array_push($datalist2, $datalist[$i]);
}

if (isset($datalist[0])) {
    $response->code = 200;
    $response->message = 'result';
    $response->data = $datalist2;
    $response->json();
    die();
} else {
    $response->code = 200;
    $response->message = 'Tidak ada data ditampilkan.';
    $response->data = '';
    $response->json();
    die();
}
