<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$iduser = $_GET['iduser'];

$datalist = array();
$data = $conn->query("SELECT * FROM user_voucher_mandiri a 
JOIN transaksi_voucher_mandiri b ON a.idtransaksi_voucher_mandiri = b.idtransaksi_voucher_mandiri 
WHERE b.tanggal_mulai <= CURRENT_TIME() AND b.tanggal_selesai >= CURRENT_TIME() AND a.status_pakai = '0' AND b.qty_voucher_sisa != 0 AND a.iduser = '$iduser' ORDER BY a.tanggal_input DESC");
foreach ($data as $key => $value) {
    array_push($datalist, array(
        'iduser_voucher_mandiri' => $value['iduser_voucher_mandiri'],
        'nama_voucher' => $value['nama_voucher'],
        'deskripsi_voucher' => $value['deskripsi_voucher'],
        'jenis_potongan' => $value['jenis_potongan'],
        'nilai_potongan' => $value['nilai_potongan'],
        'tanggal_mulai' => $value['tanggal_mulai'],
        'tanggal_selesai' => $value['tanggal_selesai'],
        'qty_voucher_sisa' => $value['qty_voucher_sisa'],
    ));
}

if (isset($datalist[0])) {
    $response->code = 200;
    $response->message = 'result';
    $response->data = $datalist;
    $response->json();
    die();
} else {
    $response->code = 200;
    $response->message = 'No data is displayed.';
    $response->data = [];
    $response->json();
    die();
}
