<?php
require_once "../config/koneksi.php";
include "response.php";

$iduser = $_GET['iduser'] ?? '';
$idtransaction = $_GET['idtransaction'] ?? '';

$response = new Response();

if (!empty($idtransaction)) {
    $data = mysqli_fetch_object($conn->query("SELECT * FROM transaksi WHERE idtransaksi = '$idtransaction';"));
    if ($data->status_transaksi == '7') {
        $datas = mysqli_fetch_object($conn->query("
        SELECT * FROM transaksi t 
        JOIN transaksi_detail td ON t.idtransaksi = td.idtransaksi
        JOIN event e ON e.idevent = td.idevent 
        WHERE t.idtransaksi = '$idtransaction'
        "));

        $result['idtransaction'] = $datas->idtransaksi;
        $result['invoice'] = $datas->invoice;
        $result['tanggal_input'] = $datas->tanggal_input;
        $result['status_transaksi'] = switchStatus($data->status_transaksi);
        $result['kode_transaksi'] = $datas->status_transaksi;
        $result['nama_event'] = $datas->nama_event;
        $result['status_payment'] = $datas->status_payment;
        $result['payment_type'] = $datas->payment_type;
        $result['deskripsi_event'] = $datas->deskripsi_event;
        $result['url_image_panjang'] = $getimageevent . $datas->url_image_panjang;
        $result['url_image_kotak'] = $getimageevent . $datas->url_image_kotak;

        $response->code = 200;
        $response->message = 'oke';
        $response->data = $result;
        $response->json();
        die();
    }
} else {
    $data = $conn->query("SELECT t.idtransaksi,t.invoice, t.tanggal_input, t.status_transaksi, t.status_payment, t.batas_pembayaran, t.total_harga_ticket_akhir, e.nama_event, e.url_image_panjang, e.url_image_kotak FROM event e
    JOIN transaksi_detail td ON td.idevent = e.idevent
    JOIN transaksi t ON td.idtransaksi = t.idtransaksi
    WHERE t.iduser = '$iduser' ORDER BY t.tanggal_input DESC");

    $result = array();
    foreach ($data as $key => $value) {

        array_push($result, array(
            'idtransaction'     => $value['idtransaksi'],
            'invoice'           => $value['invoice'],
            'tanggal_input'     => $value['tanggal_input'],
            'status_transaksi'  => switchStatus($value['status_transaksi']),
            'kode_transaksi'    => $value['status_transaksi'],
            'status_payment'    => $value['status_payment'],
            'batas_pembayaran'  => $value['batas_pembayaran'],
            'total_harga_akhir' => $value['total_harga_ticket_akhir'],
            'nama_event'        => $value['nama_event'],
            'url_image_panjang' => $getimageevent . $value['url_image_panjang'],
            'url_image_kotak' => $getimageevent . $value['url_image_kotak']
        ));
    }

    $response->code = 200;
    $response->message = 'found';
    $response->data = $result;
    $response->json();
    die();
}


function switchStatus($status)
{
    switch ($status) {
        case '1':
            $status_transaksi = "Waiting For Payment";
            break;
        case '2':
            $status_transaksi = "Waiting for Payment Verification";
            break;
        case '3':
            $status_transaksi = "Payment Successfully";
            break;
        case '4':
            $status_transaksi = "Incomplete payment";
            break;
        case '5':
            $status_transaksi = "Sending";
            break;
        case '6':
            $status_transaksi = "Received";
            break;
        case '7':
            $status_transaksi = "Transaction Complete";
            break;
        case '8':
            $status_transaksi = "Expired";
            break;
        case '9':
            $status_transaksi = "Canceled";
            break;
        case '10':
            $status_transaksi = "Payment declined";
            break;
        default:
            $status_transaksi = "Not defined";
            break;
    }
    return $status_transaksi;
}
