<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$data = json_decode(file_get_contents('php://input'));


$data ?? sendError();
$assessment = mysqli_fetch_object($conn->query("
SELECT * FROM assessment WHERE idassessment = '$data->idassessment' AND CURRENT_TIMESTAMP <= tanggal_batas_assessment;
"));

if (is_null($assessment)) {
    $response->code = 400;
    $response->message = 'Sertifikasi sudah berakhir';
    $response->data = '';
    $response->json();
    die();
}
$idjawaban = mysqli_fetch_object($conn->query("SELECT UUID_SHORT() as id"));
$soals = $conn->query("SELECT * FROM assessment_soal_pilgan WHERE idassessment = '$data->idassessment';")->fetch_all(MYSQLI_ASSOC);

$conn->begin_transaction();

$result[] = $conn->query("INSERT into assessment_jawaban_pilgan SET idassessment_jawaban_pilgan = '$idjawaban->id',idassessment = '$data->idassessment', iduser = '$data->iduser', lama_dikerjakan = '$data->duration', idtransaksi = '$data->idtransaksi'
    ");

foreach ($data->answer as $key) {
    $result[] = $conn->query("
        INSERT into assessment_jawaban_pilgan_detail SET idassessment_jawaban_pilgan_detail = UUID_SHORT(), jawaban = '$key->answer',
        idassessment_jawaban_pilgan = '$idjawaban->id', idassessment_soal_pilgan = '$key->idsoal'
    ");

    $indexofsoal = array_search($key->idsoal, array_column($soals, 'idassessment_soal_pilgan'));
    $keyanswer = strtoupper($soals[$indexofsoal]['jawaban']);
    $useranswer = strtoupper($key->answer);

    if ($keyanswer == $useranswer) {
        $res[] = "true";
    } else {
        $res[] = "false";
    }
}

if (count($data->answer) < $assessment->tampil_assessment_pilgan) {
    for ($i = count($data->answer); $i < $assessment->tampil_assessment_pilgan; $i++) {
        $res[] = "false";
    }
}


$nilaibenar = array_count_values($res)["true"] ?? 0;
$nilai = round(($nilaibenar / $assessment->tampil_assessment_pilgan) * 100);

$result[] = $conn->query("UPDATE assessment_jawaban_pilgan SET nilai = '$nilai' WHERE idassessment_jawaban_pilgan = '$idjawaban->id'");

if (in_array(false, $result)) {
    $response->code = 400;
    $response->message = mysqli_error($conn);
    $response->data = '';
    $response->json();
    die();
} else {
    $pilgan = mysqli_fetch_object($conn->query("SELECT * FROM assessment_jawaban_pilgan ajp
    LEFT JOIN transaksi tr ON ajp.idtransaksi = tr.idtransaksi
    JOIN user usr ON ajp.iduser = usr.iduser
    WHERE idassessment_jawaban_pilgan = '$idjawaban->id'"));
    $kriteria = new Kriteria();
    $kriteria->nilai = $pilgan->nilai;
    $kriteria->obj = $conn->query("SELECT * FROM kriteria_nilai ORDER BY batas_ambang DESC;")->fetch_all(MYSQLI_ASSOC);

    $resp['idassessment'] = $assessment->idassessment;
    $resp['nama_assessment'] = $assessment->nama_assessment;
    $resp['nilai'] = $pilgan->nilai;
    $resp['comment'] = $kriteria->getDeskripsi();
    $resp['hasil'] = $kriteria->getHasil();
    $resp['duration'] = $pilgan->lama_dikerjakan;
    $resp['nama_sertifikat'] = $pilgan->nama_sertifikat;
    $resp['nama_user'] = $pilgan->nama;
    $resp['submit_at'] = $pilgan->tgl_input_time;


    $conn->commit();
    $response->code = 200;
    $response->message = 'submit success';
    $response->data = $resp;
    $response->json();
    die();
}



function sendError()
{
    $response = new Response();
    $response->code = 400;
    $response->message = 'bad request';
    $response->data = '';
    $response->json();
    die();
}

class Kriteria
{
    public $nilai;
    public $obj;
    private $hasil, $deskripsi;

    function getHasil()
    {
        foreach ($this->obj as $key => $value) {
            if ($this->nilai <= $value['batas_ambang']) {
                $this->hasil = $value['keterangan'];
                $this->deskripsi = $value['deskripsi'];
            }
        }
        return $this->hasil;
    }

    function getDeskripsi()
    {
        foreach ($this->obj as $key => $value) {
            if ($this->nilai <= $value['batas_ambang']) {
                $this->hasil = $value['keterangan'];
                $this->deskripsi = $value['deskripsi'];
            }
        }
        return $this->deskripsi;
    }
}
