<?php
require_once "../config/koneksi.php";
include "response.php";

$iduser = $_GET['iduser'] ?? '';
$response = new Response();

$data = $conn->query("SELECT t.nama_sertifikat, a.idevent_sertifikat, j.nama_event, j.url_image_panjang, a.link_sertifikat, a.tanggal_input FROM event_sertifikat a
JOIN transaksi t ON a.idtransaksi = t.idtransaksi
JOIN event j ON a.idevent = j.idevent
WHERE t.iduser = '$iduser' AND j.event_selesai <= CURRENT_TIMESTAMP AND a.status_sertifikat = 'Y' GROUP BY a.idevent_sertifikat ORDER BY t.tanggal_input DESC");
$result = array();
foreach ($data as $key => $value) {
    array_push($result, array(
        'idevent_sertifikat' => $value['idevent_sertifikat'],
        'nama_event' => $value['nama_event'],
        'nama_daftar' => $value['nama_sertifikat'],
        'url_sertifikat_depan_template' => $getimageevent . $value['url_image_panjang'],
        'link_sertifikat' => $value['link_sertifikat'],
        'tanggal_input' => $value['tanggal_input'],
    ));
}

$response->code = 200;
$response->message = "result";
$response->data = $result;
$response->json();
die();
