<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$tag   = $_GET['tag'] ?? '';

switch ($tag) {
    case "event":
        $idkategori   = $_GET['idkategori'] ?? '';
        if (empty($idkategori)) {
            // $data = $conn->query("SELECT * FROM event_kategori_master");
            // $data = $data->fetch_all(MYSQLI_ASSOC);

            $listdata = array();
            $event = $conn->query("SELECT * FROM event_kategori_master");
            foreach ($event as $key => $value) {
                array_push($listdata, array(
                    'idevent_kategori_master' => $value['idevent_kategori_master'],
                    'judul_kategori_master' => $value['judul_kategori_master'],
                    'subjudul_kategori_master' => $value['subjudul_kategori_master'],
                    'icon' => $getkategoriicon . $value['icon'],
                    'color' => $value['color'],
                ));
            }

            if (isset($listdata[0])) {
                $response->code = 200;
                $response->message = 'result';
                $response->data = $listdata;
                $response->json();
                die();
            } else {
                $response->code = 200;
                $response->message = 'Tidak ada data ditampilkan.';
                $response->data = [];
                $response->json();
                die();
            }
        } else {
            $category = array();
            $data = $conn->query("SELECT * FROM event_kategori  WHERE idevent_kategori_master = '$idkategori'");
            while ($key = mysqli_fetch_object($data)) {
                array_push($category, array(
                    'dievent_kategori' => $key->dievent_kategori,
                    'idevent_kategori_master' => $key->idevent_kategori_master,
                    'nama_kategori' => $key->nama_kategori,
                    'icon' => $getkategoriiconsub . $key->icon,
                ));
            }

            // if (isset($data[0])) {
            $response->code = 200;
            $response->message = 'result';
            $response->data = $category;
            $response->json();
            die();
            // } else {
            //     $response->code = 200;
            //     $response->message = 'Tidak ada data ditampilkan.';
            //     $response->data = [];
            //     $response->json();
            //     die();
            // }
        }
        break;
    case "assessment":
        $idkategori   = $_GET['idkategori'] ?? '';
        if (empty($idkategori)) {
            // $data = $conn->query("SELECT * FROM assessment_kategori_master");
            // $data = $data->fetch_all(MYSQLI_ASSOC);

            $listdata = array();
            $assessment = $conn->query("SELECT * FROM assessment_kategori_master");
            foreach ($assessment as $key => $value) {
                array_push($listdata, array(
                    'idassessment_kategori_master' => $value['idassessment_kategori_master'],
                    'judul_kategori_master' => $value['judul_kategori_master'],
                    'subjudul_kategori_master' => $value['subjudul_kategori_master'],
                    'icon' => $getkategoriicon . $value['icon'],
                    'color' => $value['color'],
                ));
            }

            if (isset($listdata[0])) {
                $response->code = 200;
                $response->message = 'result';
                $response->data = $listdata;
                $response->json();
                die();
            } else {
                $response->code = 200;
                $response->message = 'Tidak ada data ditampilkan.';
                $response->data = [];
                $response->json();
                die();
            }
        } else {
            $data = $conn->query("SELECT * FROM assessment_kategori  WHERE idassessment_kategori_master = '$idkategori'");
            $category = array();
            while ($key = mysqli_fetch_object($data)) {
                array_push($category, array(
                    'idassessment_kategori' => $key->idassessment_kategori,
                    'idassessment_kategori_master' => $key->idassessment_kategori_master,
                    'nama_kategori' => $key->nama_kategori,
                    'icon' => $getkategoriiconsub . $key->icon,
                ));
            }

            // if (isset($data[0])) {
            $response->code = 200;
            $response->message = 'result';
            $response->data = $category;
            $response->json();
            die();
            // } else {
            //     $response->code = 200;
            //     $response->message = 'Tidak ada data ditampilkan.';
            //     $response->data = [];
            //     $response->json();
            //     die();
            // }
        }
        break;
}
mysqli_close($conn);
