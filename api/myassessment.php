<?php
require_once "../config/koneksi.php";
include "response.php";

$iduser = $_GET['iduser'] ?? '';
$idtransaksi = $_GET['idtransaksi'] ?? '';
$response = new Response();

if (!empty($idtransaksi)) {
    $transaksi = $conn->query("SELECT * FROM transaksi_detail WHERE idtransaksi = '$idtransaksi'")->fetch_assoc();

    $cekmateri = $conn->query("SELECT * FROM assessment_materi WHERE idassessment = '$transaksi[idassessment]'")->num_rows;
    if ($cekmateri > 0) {
        $status_materi = 'Y';
    } else {
        $status_materi = 'N';
    }

    $cekquestions = $conn->query("SELECT * FROM assessment WHERE idassessment = '$transaksi[idassessment]'")->fetch_assoc();
    if (($cekquestions['tampil_assessment_pilgan'] != 0) && ($cekquestions['tampil_assessment_essay'] == 0)) {
        $status_questions = '1';
    } else if (($cekquestions['tampil_assessment_pilgan'] == 0) && ($cekquestions['tampil_assessment_essay'] != 0)) {
        $status_questions = '2';
    } else {
        $status_questions = '3';
    }

    //cek tanggal assessment sudah mulai waktu nya belum
    $cekbolehdikerjakan = mysqli_fetch_object($conn->query("SELECT COUNT(t.idtransaksi) as worked FROM assessment a
    JOIN transaksi_detail td ON a.idassessment = td.idassessment 
    JOIN transaksi t ON td.idtransaksi = t.idtransaksi
    WHERE t.iduser = '$iduser' AND t.idtransaksi = '$idtransaksi' AND t.status_transaksi = '7' AND CURRENT_TIMESTAMP() <= a.tanggal_mulai_assessment"));

    if ($status_questions == '1') {
        $cektanggalpengerjaan = $conn->query("SELECT * FROM transaksi tr 
        JOIN assessment_jawaban_pilgan ajp ON  tr.idtransaksi = ajp.idtransaksi
        WHERE tr.idtransaksi = '$idtransaksi'")->num_rows;
    } else if ($status_questions == '2') {
        $cektanggalpengerjaan = $conn->query("SELECT * FROM transaksi tr 
        JOIN assessment_jawaban_essay ajp ON  tr.idtransaksi = ajp.idtransaksi
        WHERE tr.idtransaksi = '$idtransaksi'")->num_rows;
    }

    if ($cekbolehdikerjakan->worked > 0) {
        $status_pengerjaan = '1';
        $status_lulus = 'Not Working Yet';
    } else {
        if ($cektanggalpengerjaan > 0) {
            $status_pengerjaan = '3';
            $status_lulus = 'Already Working';
        } else {
            $status_pengerjaan = '2';
            $status_lulus = 'Not Working Yet';
        }
    }

    $datalist = array();
    $data = $conn->query("SELECT * FROM assessment a
    JOIN transaksi_detail td ON a.idassessment = td.idassessment 
    JOIN transaksi t ON td.idtransaksi = t.idtransaksi
    LEFT JOIN user_bo u ON u.iduser_bo = a.iduser_bo
    LEFT JOIN kategori_user_bo c ON u.idkategori = c.idkategori
    WHERE t.iduser = '$iduser' AND td.idassessment = '$transaksi[idassessment]' AND t.status_transaksi = '7' AND CURRENT_TIMESTAMP() <= a.tanggal_batas_assessment");

    $listsponsor = array();
    $idassessment = $transaksi['idassessment'];
    $sponsores = $conn->query("SELECT * FROM sponsor WHERE idassessment = '$idassessment'");
    foreach ($sponsores as $key => $value) {
        array_push($listsponsor, array(
            'idsponsor' => $value['idsponsor'],
            'nama_sponsor' => $value['nama_sponsor'],
            'url_logo_sponsor' => $getimagesponsor . $value['icon_sponsor'],
        ));
    }

    // $sponsores = $conn->query("SELECT * FROM sponsor WHERE idassessment = '$transaksi[idassessment]'")->fetch_all(MYSQLI_ASSOC);

    // $cekpengerjaan = $conn->query("SELECT COUNT(a.idassessment_jawaban_pilgan_detail) as jawaban FROM assessment_jawaban_pilgan_detail a JOIN assessment_jawaban_pilgan b 
    // ON a.idassessment_jawaban_pilgan = b.idassessment_jawaban_pilgan WHERE b.idtransaksi = '$idtransaksi'")->fetch_assoc();
    // if ($cekpengerjaan['jawaban'] != '0') {
    //     $nilai = mysqli_fetch_object($conn->query("SELECT ajp.nilai FROM transaksi_detail td
    //     JOIN assessment_jawaban_pilgan ajp ON td.idassessment = ajp.idassessment
    //     WHERE td.idtransaksi = '$idtransaksi' AND ajp.idtransaksi = '$idtransaksi'"));
    //     if ($nilai->nilai > 30) {
    //         $status_lulus = 'Already Working';
    //     } else {
    //         $status_lulus = 'Already Working';
    //     }
    // } else {
    //     $status_lulus = 'Not Working Yet';
    // }

    foreach ($data as $key => $value) {
        if ($value['diskon_assessment'] == 0) {
            $harga_diskon = 0;
        } else {
            $harga_diskon = $value['harga_assessment'] - $value['harga_assessment'] * ($value['diskon_assessment'] / 100);
        }

        $data1['idassessment'] = $value['idassessment'];
        $data1['idtransaksi'] = $idtransaksi;
        $data1['nama_assessment'] = $value['nama_assessment'];
        $data1['deskripsi_assessment'] = $value['deskripsi_assessment'];
        $data1['tanggal_mulai_assessment'] = $value['tanggal_mulai_assessment'];
        $data1['tanggal_batas_assessment'] = $value['tanggal_batas_assessment'];
        $data1['url_sertifikat_depan_template'] = $getimagesertifikat . $value['url_sertifikat_depan_template'];
        $data1['url_image_panjang'] = $getimageassessment . $value['url_image_panjang'];
        $data1['url_image_kotak'] = $getimageassessment . $value['url_image_kotak'];
        $data1['url_logo_sponsor'] = $getimagesponsor . $value['url_logo_sponsor'];
        $data1['harga_assessment'] = $value['harga_assessment'];
        $data1['diskon_assessment'] = $value['diskon_assessment'];
        $data1['harga_diskon'] = (string)$harga_diskon;
        $data1['tampil_assessment_pilgan'] = $value['tampil_assessment_pilgan'];
        $data1['tampil_assessment_essay'] = $value['tampil_assessment_essay'];
        $data1['penulis'] = $value['nama_kategori'];
        $data1['sponsor'] = $listsponsor;
        $data1['status_lulus'] = $status_lulus;
        $data1['status_materi'] = $status_materi;
        $data1['waktu_pengerjaan'] = (string)(60 * (int)$value['waktu_pengerjaan']);
        $data1['status_pengerjaan'] = $status_pengerjaan;
        $data1['nama_daftar'] = $value['nama_sertifikat'];
        $data1['point'] = $value['point_assessment'] . " " . $value['point_assessment_nama'];
        $data1['status_questions'] = $status_questions;
    }

    if ($data) {
        $data = $data->fetch_assoc();
        $response->code = 200;
        $response->message = 'success';
        $response->data = $data1;
        $response->json();
        die();
    } else {
        $response->code = 200;
        $response->message = mysqli_error($conn);
        $response->data = [];
        $response->json();
        die();
    }
} else {

    $data2 = $conn->query("SELECT t.idtransaksi, a.idassessment, a.nama_assessment, t.nama_sertifikat, a.tanggal_mulai_assessment, a.tanggal_batas_assessment, a.url_image_panjang, a.deskripsi_assessment FROM assessment a
        JOIN transaksi_detail td ON a.idassessment = td.idassessment 
        JOIN transaksi t ON td.idtransaksi = t.idtransaksi
        WHERE t.iduser = '$iduser' AND t.status_transaksi = '7' AND CURRENT_TIMESTAMP() <= a.tanggal_batas_assessment ORDER BY t.tanggal_input DESC");

    $result = array();

    foreach ($data2 as $key => $value) {
        array_push($result, array(
            'idtransaksi' => $value['idtransaksi'],
            'idassessment' => $value['idassessment'],
            'nama' => $value['nama_assessment'],
            'nama_daftar' => $value['nama_sertifikat'],
            'tanggal_mulai' => $value['tanggal_mulai_assessment'],
            'tanggal_selesai' => $value['tanggal_batas_assessment'],
            'url_image_panjang' => $getimageassessment . $value['url_image_panjang'],
            'deskripsi' => $value['deskripsi_assessment'],
        ));
    }

    $response->code = 200;
    $response->message = "result";
    $response->data = $result;
    $response->json();
    die();
}

function switchStatus($status)
{
    switch ($status) {
        case '1':
            $status_transaksi = "Menunggu Pembayaran";
            break;
        case '2':
            $status_transaksi = "Menunggu Verifikasi Pembayaran";
            break;
        case '3':
            $status_transaksi = "Pembayaran Berhasil";
            break;
        case '4':
            $status_transaksi = "Pembayaran tidak lengkap";
            break;
        case '5':
            $status_transaksi = "Dikirim";
            break;
        case '6':
            $status_transaksi = "Diterima";
            break;
        case '7':
            $status_transaksi = "Transaksi Selesai";
            break;
        case '8':
            $status_transaksi = "Kadaluarsa";
            break;
        case '9':
            $status_transaksi = "Dibatalkan";
            break;
        case '10':
            $status_transaksi = "Pembayaran ditolak";
            break;
        default:
            $status_transaksi = "Belum didefinisikan";
            break;
    }
    return $status_transaksi;
}
