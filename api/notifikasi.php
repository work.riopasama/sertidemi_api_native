<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$iduser = $_GET['iduser'];

// if (isset($_GET[''])) {
// } else {
// }

$datalist = array();
$data = $conn->query("SELECT a.idnotifikasi, a.idtransaksi, a.status_baca, b.jenis_transaksi  FROM notifikasi a 
JOIN transaksi b ON a.idtransaksi = b.idtransaksi 
JOIN transaksi_detail f ON f.idtransaksi = b.idtransaksi 
JOIN user c ON a.iduser = c.iduser
WHERE a.iduser = '$iduser'");
foreach ($data as $key => $value) {
    if ($value['jenis_transaksi'] == 'event') {
        $data = $conn->query("SELECT a.idnotifikasi, a.idtransaksi, a.status_baca, b.jenis_transaksi  FROM notifikasi a 
JOIN transaksi b ON a.idtransaksi = b.idtransaksi 
JOIN transaksi_detail f ON f.idtransaksi = b.idtransaksi 
JOIN user c ON a.iduser = c.iduser 
LEFT JOIN event d ON d.idevent = f.idevent
WHERE a.iduser = '$iduser'");
    } else if ($value['jenis_transaksi'] == 'assessment') {
        $data = $conn->query("SELECT a.idnotifikasi, a.idtransaksi, a.status_baca, b.jenis_transaksi  FROM notifikasi a 
JOIN transaksi b ON a.idtransaksi = b.idtransaksi 
JOIN transaksi_detail f ON f.idtransaksi = b.idtransaksi 
JOIN user c ON a.iduser = c.iduser 
LEFT JOIN assessment e ON f.idassessment = e.idassessment 
WHERE a.iduser = '$iduser'");
    } else {
    }
    array_push($datalist, array(
        'idnotifikasi' => $value['idnotifikasi'],
        'idtransaksi' => $value['idtransaksi'],
        'status_baca' => $value['status_baca'],
        'jenis_transaksi' => $value['jenis_transaksi'],
    ));
}

if (isset($datalist[0])) {
    $response->code = 200;
    $response->message = 'found';
    $response->data = $datalist;
    $response->json();
    die();
} else {
    $response->code = 200;
    $response->message = mysqli_error($conn);
    $response->data = [];
    $response->json();
    die();
}
