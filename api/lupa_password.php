<?php 
require_once('../config/koneksi.php');
require 'PHPMailerold/PHPMailerAutoload.php';
include "response.php";
$response = new Response();

$email_login = $_POST['email_login'];
$referal = generate_referal_lagi();
$referal_hash = password_hash($referal, PASSWORD_DEFAULT);

$cekemail = mysqli_query($conn, "SELECT * FROM user WHERE login_email = '$email_login' AND status_aktif = 'Y'")->num_rows;
if ($cekemail > 0){
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPOptions = array(
		'ssl' => array(
			'verify_peer' => false,
			'verify_peer_name' => false,
			'allow_self_signed' => true
		)
	);
	$mail->Host = 'mail.sertidemi.com'; 
	$mail->Port = 587; 
	$mail->SMTPSecure = 'tsl'; 
	$mail->SMTPAuth = true;
	$mail->Username = 'admin@sertidemi.com'; 
	$mail->Password = 'A123123123b@'; 
	$mail->setFrom('admin@sertidemi.com', 'Sertidemi');
	$mail->addAddress($email_login, $email_login);
	$mail->isHTML(true);
	$mail->Subject = 'forgot password';

	$mail->Body = '
	<!DOCTYPE html>
	<html>
	<head>
	<title>Forgot Password</title>
	<!-- FONT -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
	</head>
	<body style="background:#f3f2ef;font-style: oblique;">
	<div class="email-check" style="max-width:500px; margin:50px auto; padding:20px; background:#fff;border-radius:3px; -webkit-box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.75);-moz-box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.75); box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.3);">
	<div class="email-container">
	<center><h3>Forgot Password</h3></center>
	<hr><br>
	Hallo, '.$email_login.'.
	<br><br>            
	<div align="justify">
	You have been request to change the password on Sertidemi application.<br>
	Please copy this password below to login on application.<br><br>
	<center><b style="font-family: sans-serif; color:red;">New Password : '.$referal.'</b></center><br>
	Enjoy our best service to enjoy the event app more.<br>
	<br>
	For more information about our services :<br>
	<table style="margin-left: 25px;">
	<tr>
	<td style="padding-right: 15px" nowrap=""> 
	Call Center
	</td>
	<td>
	: +62811-2845-174
	</td>
	</tr>
	<tr>
	<td> 
	Website
	</td>
	<td>
	: www.sertidemi.com
	</td>
	</tr>
	<tr>
	<td style="padding-right: 15px"> 
	Addres
	</td>
	<td>
	: Jl. Beo No.38-40, Mrican, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281
	</td>
	</tr>
	</table><br>
	<div style="text-align: left">
	Best regards,<br><br><br>
	<b style="font-family: sans-serif; color:#21325E;"> SERTIDEMI</b>.<br>
	</div>
	</div>
	</div>
	</body>
	</html>';

	if(!$mail->send()) {
        $response->code = 400;
        $response->message = 'Email not sent. Please try again or contact our admin.';
        $response->data = "";
        $response->json();
        die();
	} else {
		$query = mysqli_query($conn, "UPDATE user SET login_password = '$referal_hash' WHERE login_email = '$email_login'");

        $response->code = 200;
        $response->message = 'Please check your email that have been registered. A new password will be sent to your email. Check inbox / spam / promotion / update folders.';
        $response->data = "";
        $response->json();
        die();
	}

} else {
    $response->code = 400;
    $response->message = 'No email registered in the Sertidemi app.';
    $response->data = "";
    $response->json();
    die();
}

mysqli_close($conn);

?>