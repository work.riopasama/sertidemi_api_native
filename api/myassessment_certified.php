<?php
require_once "../config/koneksi.php";
include "response.php";

$iduser = $_GET['iduser'] ?? '';
$response = new Response();

$data = $conn->query("(SELECT t.nama_sertifikat, a.idassessment_sertifikat, j.nama_assessment, s.tgl_input_time, s.nilai, j.url_image_panjang, a.link_sertifikat 
FROM assessment_sertifikat a
	JOIN transaksi t ON a.idtransaksi = t.idtransaksi
    LEFT JOIN assessment_jawaban_pilgan s ON t.idtransaksi = s.idtransaksi
    JOIN assessment j ON s.idassessment = j.idassessment
    WHERE t.iduser = '$iduser' AND t.status_transaksi = '7' AND s.iduser = '$iduser'  GROUP BY a.idtransaksi)
    UNION 
    (SELECT t.nama_sertifikat, a.idassessment_sertifikat, j.nama_assessment, s.tgl_input_time, s.nilai, j.url_image_panjang, a.link_sertifikat 
FROM assessment_sertifikat a
	JOIN transaksi t ON a.idtransaksi = t.idtransaksi
    LEFT JOIN assessment_jawaban_essay s ON t.idtransaksi = s.idtransaksi
    JOIN assessment j ON s.idassessment = j.idassessment
    WHERE t.iduser = '$iduser' AND t.status_transaksi = '7' AND s.iduser = '$iduser' AND s.koreksi_admin = '2' GROUP BY a.idtransaksi) ORDER BY tgl_input_time DESC");
$result = array();
foreach ($data as $key => $value) {
    array_push($result, array(
        'idassessment_sertifikat' => $value['idassessment_sertifikat'],
        'nama_assessment' => $value['nama_assessment'],
        'nama_daftar' => $value['nama_sertifikat'],
        'tgl_input_time' => $value['tgl_input_time'],
        'nilai' => $value['nilai'],
        'url_sertifikat_depan_template' => $getimageassessment . $value['url_image_panjang'],
        'link_sertifikat' => $value['link_sertifikat'],
    ));
}

$response->code = 200;
$response->message = "result";
$response->data = $result;
$response->json();
die();

function switchStatus($status)
{
    switch ($status) {
        case '1':
            $status_transaksi = "Menunggu Pembayaran";
            break;
        case '2':
            $status_transaksi = "Menunggu Verifikasi Pembayaran";
            break;
        case '3':
            $status_transaksi = "Pembayaran Berhasil";
            break;
        case '4':
            $status_transaksi = "Pembayaran tidak lengkap";
            break;
        case '5':
            $status_transaksi = "Dikirim";
            break;
        case '6':
            $status_transaksi = "Diterima";
            break;
        case '7':
            $status_transaksi = "Transaksi Selesai";
            break;
        case '8':
            $status_transaksi = "Kadaluarsa";
            break;
        case '9':
            $status_transaksi = "Dibatalkan";
            break;
        case '10':
            $status_transaksi = "Pembayaran ditolak";
            break;
        default:
            $status_transaksi = "Belum didefinisikan";
            break;
    }
    return $status_transaksi;
}
