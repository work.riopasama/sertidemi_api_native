<?php
require_once('../config/koneksi.php');
include "response.php";

$tag = $_GET['tag'];
$id = $_GET['id'] ?? '';
$q  = $_GET['q'] ?? '';
$filter  = $_GET['filter'] ?? '';

switch ($tag) {
    case "transaction":
        $response = new Response();
        if (empty($id)) {
            $response->code = 400;
            $response->message = 'bad request';
            $response->data = [];
            $response->json();
            die();
        } else {
            $datalist = array();
            $data = $conn->query("SELECT * FROM transaksi_voucher WHERE (idevent = '$id' OR idassessment = '$id') AND masa_aktif_voucher >= CURRENT_TIME()");
            foreach ($data as $key => $value) {
                if (!empty($value['idevent'])) {
                    $status = 'event';
                } else if (!empty($value['idassessment'])) {
                    $status = 'assessment';
                }
                array_push($datalist, array(
                    'idtransaksi_voucher' => $value['idtransaksi_voucher'],
                    'nama_voucher' => $value['nama_voucher'],
                    'deskripsi_voucher' => $value['deskripsi_voucher'],
                    'jenis_potongan' => $value['jenis_potongan'],
                    'nilai_potongan' => $value['nilai_potongan'],
                    'masa_aktif_voucher' => $value['masa_aktif_voucher'],
                    'status' => $status
                ));
            }

            if (isset($datalist[0])) {
                $response->code = 200;
                $response->message = 'result';
                $response->data = $datalist;
                $response->json();
                die();
            } else {
                $response->code = 200;
                $response->message = 'Tidak ada data ditampilkan.';
                $response->data = [];
                $response->json();
                die();
            }
        }
        break;
    case "profile":
        $response = new Response();
        switch ($filter) {
            case "":
                if (empty($q)) {
                    $datalist = array();
                    $data = $conn->query("SELECT * FROM transaksi_voucher WHERE masa_aktif_voucher >= CURRENT_TIME()");
                } else {
                    $datalist = array();
                    $data = $conn->query("SELECT * FROM transaksi_voucher WHERE nama_voucher LIKE '%$q%' AND masa_aktif_voucher >= CURRENT_TIME()");
                }
                break;
            case "event":
                if (empty($q)) {
                    $datalist = array();
                    $data = $conn->query("SELECT * FROM transaksi_voucher WHERE idevent IS NOT NULL AND masa_aktif_voucher >= CURRENT_TIME()");
                } else {
                    $datalist = array();
                    $data = $conn->query("SELECT * FROM transaksi_voucher WHERE idevent IS NOT NULL AND nama_voucher LIKE '%$q%' AND masa_aktif_voucher >= CURRENT_TIME()");
                }
                break;
            case "assessment":
                if (empty($q)) {
                    $datalist = array();
                    $data = $conn->query("SELECT * FROM transaksi_voucher WHERE idassessment IS NOT NULL AND masa_aktif_voucher >= CURRENT_TIME()");
                } else {
                    $datalist = array();
                    $data = $conn->query("SELECT * FROM transaksi_voucher WHERE idassessment IS NOT NULL AND nama_voucher LIKE '%$q%' AND masa_aktif_voucher >= CURRENT_TIME()");
                }
                break;
            default:
                if (empty($q)) {
                    $datalist = array();
                    $data = $conn->query("SELECT * FROM transaksi_voucher WHERE masa_aktif_voucher >= CURRENT_TIME()");
                } else {
                    $datalist = array();
                    $data = $conn->query("SELECT * FROM transaksi_voucher WHERE nama_voucher LIKE '%$q%' AND masa_aktif_voucher >= CURRENT_TIME()");
                }
                break;
        }

        foreach ($data as $key => $value) {
            if (!empty($value['idevent'])) {
                $status = 'event';
                $idproduk = $value['idevent'];
            } else if (!empty($value['idassessment'])) {
                $status = 'assessment';
                $idproduk = $value['idassessment'];
            }
            array_push($datalist, array(
                'idtransaksi_voucher' => $value['idtransaksi_voucher'],
                'idproduk' => $idproduk,
                'nama_voucher' => $value['nama_voucher'],
                'deskripsi_voucher' => $value['deskripsi_voucher'],
                'jenis_potongan' => $value['jenis_potongan'],
                'nilai_potongan' => $value['nilai_potongan'],
                'masa_aktif_voucher' => $value['masa_aktif_voucher'],
                'status' => $status
            ));
        }

        if (isset($datalist[0])) {
            $response->code = 200;
            $response->message = 'result';
            $response->data = $datalist;
            $response->json();
            die();
        } else {
            $response->code = 200;
            $response->message = 'Tidak ada data ditampilkan.';
            $response->data = [];
            $response->json();
            die();
        }
        break;
}
