<?php
require_once('../config/koneksi.php');
include "response.php";

$response = new Response();

$q = $_GET['q'] ?? '';
$offset = $_GET['offset'] ?? 0;

if (!empty($q)) {
    $data = $conn->query("(SELECT ass.idassessment as id, ass.nama_assessment as nama, ass.tanggal_mulai_assessment as tgl_mulai, ass.tanggal_batas_assessment as tgl_selesai, ass.harga_assessment as harga, ass.diskon_assessment as diskon, ass.status_diskon ,ass.url_image_panjang as image_panjang, ass.url_image_kotak as image_kotak, 'assessment' as status, ass.idassessment_kategori as idkategori FROM assessment ass WHERE CURRENT_TIMESTAMP <= ass.tanggal_batas_assessment AND ass.nama_assessment LIKE '%$q%' LIMIT 10 OFFSET $offset)
    UNION 
    (SELECT e.idevent as id, e.nama_event as nama, e.event_mulai as tgl_mulai, e.event_selesai as tgl_selesai, e.harga as harga, e.diskon, e.status_diskon,e.url_image_panjang as image_panjang, e.url_image_kotak as image_kotak, 'event' as status, e.idevent_kategori as idkategori FROM event e WHERE CURRENT_TIMESTAMP <= e.event_selesai AND e.nama_event LIKE '%$q%' LIMIT 10 OFFSET $offset);");

    $result = array();

    while ($key = mysqli_fetch_object($data)) {
        $harga_diskon = $key->status_diskon == "1" ? $key->harga - ($key->harga * $key->diskon / 100) : $key->harga;
        $harga_diskon = $key->status_diskon == "2" ? $key->harga - $key->diskon : $harga_diskon;

        $url_image_panjang = $key->status == "assessment" ? $getimageassessment . $key->image_panjang : $getimageevent . $key->image_panjang;
        $url_image_kotak = $key->status == "assessment" ? $getimageassessment . $key->image_kotak : $getimageevent . $key->image_kotak;

        if ($key->status == "assessment") { 
            if ($key->diskon == 0){
                $harga_diskon = 0;
            } else {
                if ($key->status_diskon == '1') {
                    $harga_diskon = $key->harga - ($key->harga * ($key->diskon / 100));
                } else if ($key->status_diskon == '2') {
                    $harga_diskon = $key->harga - $key->diskon;
                } else {
                    $harga_diskon = 0;
                }
            }
        }

        array_push($result, array(
            'id'        => $key->id,
            'nama'      => $key->nama,
            'tgl_mulai' => $key->tgl_mulai,
            'tgl_selesai' => $key->tgl_selesai,
            'harga'     => $key->harga,
            'status_diskon'     => $key->status_diskon,
            'harga_diskon' => $harga_diskon,
            'diskon' => $key->diskon,
            'image_panjang' => $url_image_panjang,
            'image_kotak' => $url_image_kotak,
            'status'    => $key->status,
            'idkategori' => $key->idkategori
        ));
    }

    if ($data) {
        $response->code = 200;
        $response->message = 'found';
        $response->data = $result;
        $response->json();
    } else {
        $response->code = 400;
        $response->message = mysqli_error($conn);
        $response->data = [];
        $response->json();
    }
    die();
}

$dataevent = $conn->query("SELECT * FROM event where NOW() <= event_selesai ORDER BY event.tanggal_input DESC LIMIT 5");
$dataeventlist = array();
foreach ($dataevent as $key => $value) {
    if ($value['status_diskon'] == '1') {
        $harga_diskon = $value['harga'] - ($value['harga'] * ($value['diskon'] / 100));
    } else if ($value['status_diskon'] == '2') {
        $harga_diskon = $value['harga'] - $value['diskon'];
    } else {
        $harga_diskon = 0;
    }
    array_push($dataeventlist, array(
        'id' => $value['idevent'],
        'nama' => $value['nama_event'],
        'mulai' => $value['event_mulai'],
        'selesai' => $value['event_selesai'],
        'harga' => $value['harga'],
        'status_diskon'     => $value['status_diskon'],
        'diskon' => $value['diskon'],
        'harga_diskon' => $harga_diskon,
        'url_image_panjang' => $getimageevent . $value['url_image_panjang'],
        'url_image_kotak' => $getimageevent . $value['url_image_kotak'],
        'status' => 'event',
    ));
}

$dataassessment = $conn->query("SELECT * FROM assessment where NOW() <= tanggal_batas_assessment ORDER BY tanggal_input DESC LIMIT 5");
$dataassessmentlist = array();
foreach ($dataassessment as $key => $value) {
    if ($value['status_diskon'] == '1') {
        $harga_diskon = $value['harga_assessment'] - ($value['harga_assessment'] * ($value['diskon_assessment'] / 100));
    } else if ($value['status_diskon'] == '2') {
        $harga_diskon = $value['harga_assessment'] - $value['diskon_assessment'];
    } else {
        $harga_diskon = 0;
    }
    array_push($dataassessmentlist, array(
        'id' => $value['idassessment'],
        'nama' => $value['nama_assessment'],
        'mulai' => $value['tanggal_mulai_assessment'],
        'selesai' => $value['tanggal_batas_assessment'],
        'harga' => $value['harga_assessment'],
        'status_diskon'     => $value['status_diskon'],
        'diskon' => $value['diskon_assessment'],
        'harga_diskon' => $harga_diskon,
        'url_image_panjang' => $getimageassessment . $value['url_image_panjang'],
        'url_image_kotak' => $getimageassessment . $value['url_image_kotak'],
        'status' => 'assessment',
    ));
}

$dataeventterlaris = $conn->query("SELECT *, (SELECT COUNT(td.idtransaksi) FROM transaksi_detail td WHERE td.idevent = e.idevent) as jml_terlaris FROM event e
WHERE NOW() <= e.event_selesai ORDER BY jml_terlaris DESC LIMIT 5");
$dataeventterlarislist = array();
foreach ($dataeventterlaris as $key => $value) {
    if ($value['status_diskon'] == '1') {
        $harga_diskon = $value['harga'] - ($value['harga'] * ($value['diskon'] / 100));
    } else if ($value['status_diskon'] == '2') {
        $harga_diskon = $value['harga'] - $value['diskon'];
    } else {
        $harga_diskon = 0;
    }
    array_push($dataeventterlarislist, array(
        'id' => $value['idevent'],
        'nama' => $value['nama_event'],
        'mulai' => $value['event_mulai'],
        'selesai' => $value['event_selesai'],
        'harga' => $value['harga'],
        'status_diskon'     => $value['status_diskon'],
        'diskon' => $value['diskon'],
        'harga_diskon' => $harga_diskon,
        'url_image_panjang' => $getimageevent . $value['url_image_panjang'],
        'url_image_kotak' => $getimageevent . $value['url_image_kotak'],
        'terjual' => $value['jml_terlaris'],
        'status' => 'event',
    ));
}

$dataassessmentterlaris = $conn->query("SELECT *, (SELECT COUNT(td.idtransaksi) FROM transaksi_detail td WHERE td.idassessment = a.idassessment) as jml_terlaris FROM assessment a
WHERE NOW() <= a.tanggal_batas_assessment ORDER BY jml_terlaris DESC LIMIT 5");
$dataassessmentterlarislist = array();
foreach ($dataassessmentterlaris as $key => $value) {
    if ($value['status_diskon'] == '1') {
        $harga_diskon = $value['harga_assessment'] - ($value['harga_assessment'] * ($value['diskon_assessment'] / 100));
    } else if ($value['status_diskon'] == '2') {
        $harga_diskon = $value['harga_assessment'] - $value['diskon_assessment'];
    } else {
        $harga_diskon = 0;
    }
    array_push($dataassessmentterlarislist, array(
        'id' => $value['idassessment'],
        'nama' => $value['nama_assessment'],
        'mulai' => $value['tanggal_mulai_assessment'],
        'selesai' => $value['tanggal_mulai_assessment'],
        'harga' => $value['harga_assessment'],
        'status_diskon'     => $value['status_diskon'],
        'diskon' => $value['diskon_assessment'],
        'harga_diskon' => $harga_diskon,
        'url_image_panjang' => $getimageassessment . $value['url_image_panjang'],
        'url_image_kotak' => $getimageassessment . $value['url_image_kotak'],
        'terjual' => $value['jml_terlaris'],
        'status' => 'assessment',
    ));
}

$result[0]['nama'] = 'Latest Event';
$result[0]['status'] = 'event';
$result[0]['url'] = 'home_other.php?tag=event';
$result[0]['data'] = $dataeventlist;
$result[1]['nama'] = 'Latest Assessment';
$result[1]['status'] = 'assessment';
$result[1]['url'] = 'home_other.php?tag=assessment';
$result[1]['data'] = $dataassessmentlist;
$result[2]['nama'] = 'Hot Event';
$result[2]['status'] = 'event';
$result[2]['url'] = 'home_other.php?tag=eventterlaris';
$result[2]['data'] = $dataeventterlarislist;
$result[3]['nama'] = 'Hot Assessment';
$result[3]['status'] = 'assessment';
$result[3]['url'] = 'home_other.php?tag=assessmentterlaris';
$result[3]['data'] = $dataassessmentterlarislist;

$response->code = 200;
$response->message = 'result';
$response->data = $result;
$response->json();
die();

mysqli_close($conn);
