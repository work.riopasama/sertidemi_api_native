<?php
require_once('../config/koneksi.php');
require('../sertifikat/fpdf.php');

$idtransaksi = $_GET['idtransaksi'];

$assessment = mysqli_query($conn, "SELECT a.nama_sertifikat, c.nama_assessment, c.tanggal_mulai_assessment, d.nilai, c.url_sertifikat_depan_template, c.url_logo_sponsor FROM transaksi a 
LEFT JOIN transaksi_detail b On a.idtransaksi = b.idtransaksi
LEFT JOIN assessment c ON b.idassessment = c.idassessment
LEFT JOIN assessment_jawaban_pilgan d ON c.idassessment = d.idassessment WHERE d.idtransaksi = '$idtransaksi' GROUP BY d.idtransaksi")->fetch_assoc();
$template_event = "../image/assessment/".$assessment['url_sertifikat_depan_template'];
$logo_sponsor = "../image/sponsor/".$assessment['url_logo_sponsor'];
if ($assessment['nilai'] <= 30){
    $ket_nilai = $assessment['nilai'].' (FAIL) ';
} else {
    $ket_nilai = $assessment['nilai'].' (PASS) ';
}

if ($assessment['nilai'] <= 30){
    $ket_predikat = 'PARTICIPANT';
} else if ($assessment['nilai'] <= 40) {
    $ket_predikat = 'BEGINNER';
} else if ($assessment['nilai'] <= 50){
    $ket_predikat = 'PIONEER';
} else if ($assessment['nilai'] <= 60){
    $ket_predikat = 'ADVANCED BEGINNERS';
} else if ($assessment['nilai'] <= 70){
    $ket_predikat = 'COMPETENT';
} else if ($assessment['nilai'] <= 80){
    $ket_predikat = 'PROFESSIONAL';
} else if ($assessment['nilai'] <= 90) {
    $ket_predikat = 'EXPERT';
} else {
    $ket_predikat = 'MASTER';
}

//$name = text to be added, $x= x cordinate, $y = y coordinate, $a = alignment , $f= Font Name, $t = Bold / Italic, $s = Font Size, $r = Red, $g = Green Font color, $b = Blue Font Color
function AddText($pdf, $text, $x, $y, $a, $f, $t, $s, $r, $g, $b) {
$pdf->SetFont($f,$t,$s);	
$pdf->SetXY($x,$y);
$pdf->SetTextColor($r,$g,$b);
$pdf->Cell(0,10,$text,0,0,$a);	
}

$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
// $pdf->SetFontSize(0);
$pdf->SetCreator('Sertidemi');
// Add background image for PDF
$pdf->Image($template_event,0,0,0);
$pdf->Image($logo_sponsor, 80, 45, -250);	
$pdf->Image($logo_sponsor, 140, 250, -300);		
$pdf->SetTitle('eSertifikat_'.$assessment['nama_sertifikat'].'_'.$assessment['nama_assessment']);
// AddText($pdf,ucwords('CERTIFICATE'), 20,72, 'C', 'times','B',35,35,35,35);
// AddText($pdf,ucwords('COMPETENCE'), 20,80, 'C', 'times','B',20,35,35,35);
// AddText($pdf,ucwords('Thank you for your participation in the assessment of Sertidemi Certification.'), 20,92, 'C', 'times','',15,35,35,35);
// AddText($pdf,ucwords('Here are the results you will get : '), 20,97, 'C', 'times','',15,35,35,35);
// AddText($pdf,ucwords('Presented to'), 20,105, 'C', 'times','',15,35,35,35);
// AddText($pdf,ucwords($assessment['nama_sertifikat']), 20,115, 'C', 'Times','B',30, 35, 35,35);
// AddText($pdf,ucwords('with test scores'), 20,125, 'C', 'times','',15,35,35,35);
// AddText($pdf,ucwords($ket_nilai), 20,135, 'C', 'times','B',23,35,35,35);
// AddText($pdf,ucwords('with predicate'), 20,145, 'C', 'times','',15,35,35,35);
// AddText($pdf,ucwords($ket_predikat), 20,155, 'C', 'times','B',23,35,35,35);
// AddText($pdf,ucwords('in the title of the exam'), 20,165, 'C', 'times','',15,35,35,35);
// AddText($pdf,ucwords($assessment['nama_assessment']), 20,175, 'C', 'times','B',20,35,35,35);
// AddText($pdf,ucwords('We CONGRATULATE all of you who have successfully passed'), 20,185, 'C', 'times','',15,35,35,35);
// AddText($pdf,ucwords('the assessment of this certification.'), 20,191, 'C', 'times','',15,35,35,35);
// AddText($pdf,ucwords('And for you who have not achieved the desired result can retake'), 20,197, 'C', 'times','',15,35,35,35);
// AddText($pdf,ucwords('the assessment as long as this certification is still
// available in the Sertidemi Apps.'), 20,203, 'C', 'times','',15,35,35,35);
// AddText($pdf,ucwords('Thankyou.'), 20,213, 'C', 'times','',15,35,35,35);
// AddText($pdf,ucwords('Thank you for your pasticipation.'), 20,219, 'C', 'times','',15,35,35,35);
AddText($pdf,ucwords('CERTIFICATE'), 20,80, 'C', 'times','B',40,35,35,35);
AddText($pdf,ucwords('COMPETENCE'), 20,90, 'C', 'times','B',25,35,35,35);
AddText($pdf,ucwords('Presented to'), 20,105, 'C', 'times','',15,35,35,35);
AddText($pdf,ucwords($assessment['nama_sertifikat']), 20,115, 'C', 'times','B',30,35,35,35);
AddText($pdf,ucwords('with test scores'), 20,130, 'C', 'times','',15,35,35,35);
AddText($pdf,ucwords($ket_nilai), 20,140, 'C', 'times','B',23,35,35,35);
AddText($pdf,ucwords('with predicate'), 20,155, 'C', 'times','',15,35,35,35);
AddText($pdf,ucwords($ket_predikat), 20,165, 'C', 'times','B',23,35,35,35);
AddText($pdf,ucwords('in the title of the exam'), 20,180, 'C', 'times','',15,35,35,35);
AddText($pdf,ucwords($assessment['nama_assessment']), 20,190, 'C', 'times','B',20,35,35,35);
AddText($pdf,ucwords('held date'), 20,205, 'C', 'times','',15,35,35,35);
AddText($pdf,ucwords(date('d F Y', strtotime($assessment['tanggal_mulai_assessment'])),), 20,215, 'C', 'times','B',20,35,35,35);
//Add a Name to the certificate
$pdf->Output('', 'eSertifikat_'.$assessment['nama_sertifikat'].'_'.$assessment['nama_assessment'].'.pdf');
